<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Payments;

Route::get('/', function () {

    //$payments = DB::table('payments')->get();
    $payments = Payments::all();
        
    return view('welcome', compact('payments')); // creates array with elements from $payments
    
    // return view('welcome')->with(compact('payments') ); 

    //return $payments;

});
