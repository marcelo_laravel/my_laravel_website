<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    public function scopeCancelled($query)
    {
        return $query->where('cancelled', 1);
    }

    public static function scopeFraudulent($query)
    {
        $suspiciousAmount = '50000'; // any payment above this amount will be considered suspicious
        $occurencies =  '2'; // how many times does the same date has to match when detecting fraud
        return $query = Payments::raw('SELECT *
                                        FROM payments
                                        WHERE amount>'.$suspiciousAmount.' AND cancelled=TRUE AND paymentDate 
                                        IN 
                                        (
                                            SELECT paymentDate
                                            FROM payments
                                            GROUP BY paymentDate
                                            HAVING COUNT(paymentDate)>'.$occurencies.'
                                        ); 
                                        ')->get(); 
    }
}

// ----------------------------------------------------------------        
// This is the query I have used for testing directly in XAMPP:
// ----------------------------------------------------------------
/*  SELECT * 
	FROM payments
	WHERE paymentDate 
	IN (
	  SELECT paymentDate
	  FROM payments
	  GROUP BY paymentDate
	  HAVING COUNT(paymentDate)>2
	) AND amount>50000 AND cancelled=TRUE */